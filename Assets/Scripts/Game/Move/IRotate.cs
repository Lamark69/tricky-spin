﻿namespace Game.Move
{
    public interface IRotate
    {
        float AngleSpeed { set; }
    }
}
