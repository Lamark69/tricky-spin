﻿using UnityEngine;

namespace Game.Move
{
    public interface IMove
    {
        Vector2 Velocity { set; }
    }
}
