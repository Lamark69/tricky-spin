﻿using UnityEngine;

namespace Game.Move
{
    public class Mover : MonoBehaviour,IMove,IRotate
    {
        [SerializeField] private Transform _rotateTransform;
        [SerializeField] private Transform _moveTransform;
        public Vector2 Velocity { get; set;}
        public float AngleSpeed { get; set;}
        
        private void Awake()
        {
            if (_rotateTransform == null)
                _rotateTransform = transform;
            if (_moveTransform == null)
                _moveTransform = transform;
        }
        
        private void Update()
        {
            _rotateTransform.rotation *= Quaternion.Euler(0,0,AngleSpeed*Time.deltaTime);
            _moveTransform.position += (Vector3)Velocity * Time.deltaTime;
        }
    }
}
