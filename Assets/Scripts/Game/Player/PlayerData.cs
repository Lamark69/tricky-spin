﻿using Common;
using Helpers;

namespace Game.Player
{
    public static class PlayerData
    {
        
        public static int Score
        {
            get => _playerScore.Value;
            set => _playerScore.Value = value;
        }
        private static IntPrefs _playerScore = new IntPrefs(Const.PlayerScoreTag, 0);
    }
}
