﻿using Game.Input;
using Game.Move;
using Game.Obstacles;
using Helpers;
using UnityEngine;

namespace Game.Player
{
    using Events;
    public class Player : MonoBehaviour,IPlayerInteraction
    {
        [SerializeField] private PlayerSettings _playerSettings;
        
        private IGameInput _gameInput;
        private IRotate _rotator;

        private void Awake()
        {
            Events.Subscribe<GameEvents.PlayerDie>(OnDie);

            _rotator = GetComponent<IRotate>();
        
            _gameInput = InterfaceHelper.FindInterface<IGameInput>();
            _gameInput.OnDown += OnInputDown;
            _gameInput.OnUp += OnInputUp;
        }

        private void OnInputDown()
        {
            _rotator.AngleSpeed = _playerSettings.AngleSpeedInputDown;
        }

        private void OnInputUp()
        {
            _rotator.AngleSpeed = _playerSettings.AngleSpeedInputUp;
        }

        private void OnDie()
        {
            _rotator.AngleSpeed = 0;
            _gameInput.OnDown -= OnInputDown;
            _gameInput.OnUp -= OnInputUp;
        }
    
        public void TouchObstacle(EnemyObstacle obstacle)
        {
            Events.SetEvent<GameEvents.PlayerTouchEnemy>(new GameEvents.PlayerTouchEnemy(){Enemy = obstacle});
        }
    
        public void TouchObstacle(CollectObstacle obstacle)
        {
            Events.SetEvent<GameEvents.PlayerCollect>(new GameEvents.PlayerCollect(){Obstacle = obstacle});
        }
    
        private void Start()
        {
            _rotator.AngleSpeed = _playerSettings.AngleSpeedInputUp;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            Obstacle obstacle = other.GetComponent<Obstacle>();
            obstacle?.Accept(this);
        }
    }
}
