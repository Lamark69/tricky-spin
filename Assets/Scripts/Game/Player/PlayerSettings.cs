﻿using UnityEngine;

[CreateAssetMenu(fileName = "PlayerSettings", menuName = "ScriptableObjects/PlayerSettings", order = 1)]
public class PlayerSettings : ScriptableObject
{
    public float AngleSpeedInputDown = 200;
    public float AngleSpeedInputUp = -200;
}
