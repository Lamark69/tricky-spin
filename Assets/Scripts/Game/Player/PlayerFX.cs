﻿using UnityEngine;

namespace Game.Player
{
    using Events;
    public class PlayerFX : MonoBehaviour
    {
        [SerializeField] private GameObject _model;
        [SerializeField] private GameObject _dieFx;
    
        private void Awake()
        {
            Events.Subscribe<GameEvents.PlayerDie>(OnDie);
        }

        private void OnDie()
        {
            _dieFx.transform.parent = null;
            _dieFx.SetActive(true);
            _model.SetActive(false);
        }
    }
}
