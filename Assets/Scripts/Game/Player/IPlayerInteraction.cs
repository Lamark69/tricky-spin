﻿using Game.Obstacles;

namespace Game.Player
{
    public interface IPlayerInteraction
    {
        void TouchObstacle(EnemyObstacle osbtacle);
        void TouchObstacle(CollectObstacle osbtacle);
    }
}
