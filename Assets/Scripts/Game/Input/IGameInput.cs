﻿using System;

namespace Game.Input
{
     public interface IGameInput
     {
          event Action OnDown;
          event Action OnUp;
     }
}
