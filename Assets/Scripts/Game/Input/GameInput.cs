﻿using System;
using UnityEngine;

namespace Game.Input
{
    public class GameInput : MonoBehaviour,IGameInput
    {
        public event Action OnDown;
        public event Action OnUp;
    
        void Update()
        {
            if(UnityEngine.Input.GetMouseButtonDown(0)) OnDown?.Invoke();
            if(UnityEngine.Input.GetMouseButtonUp(0)) OnUp?.Invoke();
        }

    }
}
