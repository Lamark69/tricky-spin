﻿using System.Collections.Generic;
using System;
using UnityEngine;

namespace Game.Events {
    internal class EventClients {
        private List<Action<object>> _clients = new List<Action<object>>();
        private Type _type;
        private object _lastEvent = null;

        public EventClients(Type type) {
            _type = type;
        }

        public void SetEvent(object arg)
        {
            _lastEvent = arg;
            _clients.ForEach(action => action?.Invoke(arg));
        }

        public void Subscribe(Action<object> action,bool instantNotify = false) {
            if (instantNotify && _lastEvent != null)
            {
                action?.Invoke(_lastEvent);
            }
            if (!_clients.Contains(action)) _clients.Add(action);
        }

        public void Unsubscribe(Action<object> action) {
            if (_clients.Contains(action)) {
                _clients.Remove(action);
            }
        }
    }

    public class Events : MonoBehaviour
    {
        public static Events Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType(typeof(Events)) as Events;
                }

                return _instance;
            }
        }
        private readonly Dictionary<Type, EventClients> _allClients = new Dictionary<Type, EventClients>();
        private static Events _instance;
        
        public static void Subscribe<T>(Action action,bool instantNotify = false) {
            Action<object> wrapperCallback = args => action();
            Subscribe<T>(wrapperCallback,instantNotify);
        }

        public static void Subscribe<T>(Action<object> action,bool instantNotify = false ) {
            EventClients manager = null;
            if (Instance._allClients.ContainsKey(typeof(T))) {
                manager = Instance._allClients[typeof(T)];
            }
            if (manager == null) {
                manager = new EventClients(typeof(T));
                Instance._allClients.Add(typeof(T), manager);
            }
            manager.Subscribe(action,instantNotify);
        }
        
        public static void SetEvent<T>(T arg = null,bool supportInstantNotify = false) where T : class, new() {
            EventClients manager = null;
            
            if(supportInstantNotify && !Instance._allClients.ContainsKey(typeof(T)))
            {
                Subscribe<T>(() => { });
            }
            
            if (Instance._allClients.ContainsKey(typeof(T))) {
                manager = Instance._allClients[typeof(T)];
            }
            manager?.SetEvent(arg ?? new T());
        }

        public static void Unsubscribe<T>(Action<object> action) {
            if (Instance == null) return;
            if (Instance._allClients.ContainsKey(typeof(T))) {
                EventClients manager = Instance._allClients[typeof(T)];
                manager?.Unsubscribe(action);
            }
        }
        
    }
}
