﻿using Game.Obstacles;
using Game.Score;

namespace Game.Events
{
    public class GameEvents
    {
        public class StartGame{}
        public class GameOver{}
        public class ContinueGame{}
    
        public class RestartGame{}
        public class PlayerDie{}

        public class PlayerCollect
        {
            public CollectObstacle Obstacle { get; set; }
        }

        public class PlayerTouchEnemy
        {
            public EnemyObstacle Enemy { get; set; }
        }

        public class PlayerScoreUpdate
        {
            public IScore Score;
        }
    }
}
