﻿using Game.Player;
using UnityEngine;

namespace Game.Score
{
    using Events;
    public class PlayerScore : MonoBehaviour,IScore
    {
        public int SessionScore => _sessionScore;
        public int MaxScore => _maxScore;

        private int _sessionScore = 0;
        private int _maxScore;
    
        private void Awake()
        {
            Events.Subscribe<GameEvents.PlayerCollect>(OnPlayerCollect);
            Events.Subscribe<GameEvents.GameOver>(OnGameOver);
        }

        private void Start()
        {
            _maxScore = PlayerData.Score;
            SendScoreEvent();
        }

        private void SendScoreEvent()
        {
            Events.SetEvent<GameEvents.PlayerScoreUpdate>(new GameEvents.PlayerScoreUpdate(){Score = this},true);
        }

        private void OnGameOver()
        {
            if(PlayerData.Score < SessionScore)
                PlayerData.Score = SessionScore; 
        }
    
        private void OnPlayerCollect(object arg)
        {
            var eventArg = arg as GameEvents.PlayerCollect;
            _sessionScore += eventArg.Obstacle.CollectPower;
            SendScoreEvent();
        }
    }
}
