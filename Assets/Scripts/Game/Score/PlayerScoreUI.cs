﻿using TMPro;
using UnityEngine;

namespace Game.Score
{
    using Game.Events;
    [RequireComponent(typeof(TMP_Text))]
    public class PlayerScoreUI : MonoBehaviour
    {
        private TMP_Text _scoreView;
    
        private void Start()
        {
            _scoreView = GetComponent<TMP_Text>();
            Events.Subscribe<GameEvents.PlayerScoreUpdate>(OnScoreUpdate);
        }

        private void OnScoreUpdate(object arg)
        {
            var eventArg = arg as GameEvents.PlayerScoreUpdate;
            _scoreView.text = $"{eventArg.Score.SessionScore}";
        }
    }
}
