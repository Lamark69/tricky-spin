﻿
namespace Game.Score
{
    public interface IScore
    {
        int SessionScore { get; }
        int MaxScore { get; }
    }
}
