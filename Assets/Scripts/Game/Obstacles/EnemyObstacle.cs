﻿using Game.Player;

namespace Game.Obstacles
{
    public class EnemyObstacle : Obstacle
    {
        public override void Accept(IPlayerInteraction playerInteraction)
        {
            playerInteraction.TouchObstacle(this);
        }
    }
}
