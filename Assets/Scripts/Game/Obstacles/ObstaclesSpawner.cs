﻿using System.Collections;
using Game.Move;
using Pool;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game.Obstacles
{
    using Events;
    public class ObstaclesSpawner : MonoBehaviour
    {
        [SerializeField] private ObstaclesSpawnSettings _spawnSettings;
        
        [SerializeField] private SimplePool _enemiesPool;
        [SerializeField] private SimplePool _obstalesPool;
        
        private bool _spawnActive = true;
    
        void Start()
        {
            _enemiesPool.Initialize();
            _obstalesPool.Initialize();
            
            Events.Subscribe<GameEvents.StartGame>(()=>StartCoroutine(SpawnEnemiesProc()));
            Events.Subscribe<GameEvents.PlayerCollect>(OnPlayerCollect);
        }

        private void OnPlayerCollect(object arg)
        {
            var eventArg = arg as GameEvents.PlayerCollect;
            _obstalesPool.ReturnObject(eventArg.Obstacle.gameObject);
        }
    
        private IEnumerator SpawnEnemiesProc()
        {
            while (_spawnActive)
            {

               GameObject spawnedObject = Random.Range(0, 1f) <= _spawnSettings.SpawnObstacleProbability ? 
                   _obstalesPool.TakeObject().gameObject : 
                   _enemiesPool.TakeObject().gameObject;
                
                spawnedObject.transform.parent = null;
                spawnedObject.transform.position = RandomPosition();
                spawnedObject.transform.rotation = quaternion.identity;
            
                IRotate rotator = spawnedObject.GetComponent<IRotate>();
                rotator.AngleSpeed = RandomAngleSpeed();
            
                IMove mover = spawnedObject.GetComponent<IMove>();
                mover.Velocity = RandomVelocity();
            
                yield return new WaitForSeconds(RandomSpawnDelay());
            }
        }

        private float RandomSpawnDelay()
        {
            return Random.Range(_spawnSettings.MinSpawnDelay, _spawnSettings.MaxSpawnDelay);
        }
        
        private Vector3 RandomPosition()
        {
            return transform.position + Vector3.right * Random.Range(-1f, 1f) * _spawnSettings.SpawnWidth;
        }

        private float RandomAngleSpeed()
        {
            return Random.Range(-1f, 1f) * _spawnSettings.MaxSpin;
        }

        private Vector3 RandomVelocity()
        {
            return _spawnSettings.MoveDirection * Random.Range(_spawnSettings.MinVelocity, _spawnSettings.MaxVelocity);
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            IPoolHandle poolHandle = other.GetComponent<IPoolHandle>();
            poolHandle?.Pool.ReturnObject(other.gameObject);
        }
    }
}
