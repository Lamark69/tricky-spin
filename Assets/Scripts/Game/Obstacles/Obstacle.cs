﻿using Game.Player;
using UnityEngine;

namespace Game.Obstacles
{
    public abstract class Obstacle : MonoBehaviour
    {
        public abstract void Accept(IPlayerInteraction playerInteraction);
    }
}
