﻿using Game.Player;
using UnityEngine;

namespace Game.Obstacles
{
    public class CollectObstacle : Obstacle
    {
        public int CollectPower => _collectPower;
        [SerializeField] private int _collectPower = 1;
    
        public override void Accept(IPlayerInteraction playerInteraction)
        {
            playerInteraction.TouchObstacle(this);
        }
    }
}
