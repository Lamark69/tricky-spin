﻿using UnityEngine;

[CreateAssetMenu(fileName = "ObstaclesSpawnSettings", menuName = "ScriptableObjects/SpawnManagerSettings", order = 1)]
public class ObstaclesSpawnSettings : ScriptableObject
{
    [Min(0)] public float MinSpawnDelay;
    [Min(0)] public float MaxSpawnDelay;
    [Min(0)] public float SpawnWidth;
    public float MaxSpin;
    public float MinVelocity;
    public float MaxVelocity;
    public Vector2 MoveDirection;
    [Range(0,1)] public float SpawnObstacleProbability;

}
