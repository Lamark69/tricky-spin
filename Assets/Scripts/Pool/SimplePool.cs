﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Pool
{
    [Serializable]
    public class SimplePool 
    {
        [SerializeField] private GameObject _prefab;
        [SerializeField] private int _size;
        [SerializeField] private Transform _parent;
    
        private List<GameObject> _pool = new List<GameObject>();
        
        public void Initialize()
        {
            for (int i = 0; i < _size; i++)
            {
                Spawn(_prefab);
            }
        }
        
        public GameObject TakeObject()
        {
            if (_pool.Count == 0)
            {
                Spawn(_prefab); 
            }
        
            GameObject item = _pool[0];
            _pool.Remove(item);

            item.gameObject.SetActive(true);
            return item;
        }


        public void ReturnObject(GameObject item)
        {
            item.transform.parent = _parent;
            item.gameObject.SetActive(false);
            _pool.Add(item);
        }
        
        private GameObject Spawn(GameObject prefab)
        {
            GameObject item = GameObject.Instantiate(prefab);
            IPoolHandle poolHandle = item.GetComponent<IPoolHandle>();
            if (poolHandle != null) poolHandle.Pool = this;
            ReturnObject(item);
            return item;
        }
    }
}
