﻿using System.Collections;
using System.Collections.Generic;
using Pool;
using UnityEngine;

public interface IPoolHandle
{
    SimplePool Pool { set; get; }
}
