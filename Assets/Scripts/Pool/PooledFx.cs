﻿using System.Collections;
using UnityEngine;

namespace Pool
{
    public class PooledFx : MonoBehaviour,IPoolHandle
    {
        public SimplePool Pool
        {
            get => _pool;
            set
            {
                _pool = value;
                Activate();
            }
        }

        private ParticleSystem _particleSystem;
        private SimplePool _pool;

        private void Awake()
        {
            _particleSystem = GetComponent<ParticleSystem>();
        }

        private void Activate()
        {
            StartCoroutine(PlayProc());
            _particleSystem.Play();
        }

        private IEnumerator PlayProc()
        {
            _particleSystem.Stop();
            _particleSystem.Play();
            yield return new WaitForSeconds(_particleSystem.main.duration);
            _particleSystem.Stop();
            Pool.ReturnObject(gameObject);
        }
    }
}
