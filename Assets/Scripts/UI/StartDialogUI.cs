﻿using Game.Player;
using TMPro;
using UnityEngine;

namespace UI
{
    public class StartDialogUI : MonoBehaviour
    {
        [SerializeField] private TMP_Text _scoreView;

        private void OnEnable()
        {
            _scoreView.text = $"{PlayerData.Score}";
        }
    }
}
