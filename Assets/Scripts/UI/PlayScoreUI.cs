﻿using TMPro;
using UnityEngine;
using Game.Events;

namespace UI
{
    public class PlayScoreUI : MonoBehaviour
    {
        [SerializeField] private TMP_Text _scoreView;
        [SerializeField] private TMP_Text _maxScoreView;

        private void Start()
        {
            Events.Subscribe<GameEvents.PlayerScoreUpdate>(OnPlayerScoreUpdate,true);
        }

        private void OnPlayerScoreUpdate(object arg)
        {
            var eventArg = arg as GameEvents.PlayerScoreUpdate;
            if(_scoreView != null)
                _scoreView.text = $"{eventArg.Score.SessionScore}";
            if(_maxScoreView != null)
                _maxScoreView.text = $"{eventArg.Score.MaxScore}";
        }
    }
}
