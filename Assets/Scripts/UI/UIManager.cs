﻿using UnityEngine;
using Game.Events;

namespace UI
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private GameObject _startDialog;
        [SerializeField] private GameObject _inGameUI;
        [SerializeField] private GameObject _gameOverDialog;
    
        private void Awake()
        {
            Events.Subscribe<GameEvents.GameOver>(OnGameOver);
        }

        private void Start()
        {
            _gameOverDialog.SetActive(false);
            _inGameUI.SetActive(false);
            _startDialog.SetActive(true);
        }

        public void OnContinue()
        {
            Events.SetEvent<GameEvents.ContinueGame>();
        }
    
        public void OnRestart()
        {
            Events.SetEvent<GameEvents.RestartGame>();
        }
    
        public  void OnGameOver()
        {
            _gameOverDialog.SetActive(true);
            _inGameUI.SetActive(false);
        }

        public void OnStart()
        {
            Events.SetEvent<GameEvents.StartGame>();
            _startDialog.SetActive(false);
            _inGameUI.SetActive(true);
        }
    

    }
}
