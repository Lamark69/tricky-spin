﻿using System.Collections;
using UnityEngine;
using Game.Events;

namespace Services
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private GameBehaviourSettings _gameBehaviourSettings;
        
        private void Awake()
        {
            Events.Subscribe<GameEvents.PlayerTouchEnemy>(OnPlayerTouchEnemy);
            Events.Subscribe<GameEvents.RestartGame>(OnRestartGame);
        }

        private void OnRestartGame()
        {
            GameLoader.RestartGame();
        }
    
        private void OnPlayerTouchEnemy(object arg)
        {
            StartCoroutine(GameOverProc());
        }

        private IEnumerator GameOverProc()
        {
            Events.SetEvent<GameEvents.PlayerDie>();
            yield return new WaitForSeconds(_gameBehaviourSettings.DelayGameOverDialog);
            Events.SetEvent<GameEvents.GameOver>();
        }
    
    }
}
