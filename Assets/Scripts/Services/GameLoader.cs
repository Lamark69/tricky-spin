﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Services
{
    public class GameLoader : MonoBehaviour
    {
        [SerializeField] private string _gameScene;
        
        void Start()
        {
            LoadGame();
        }

        private void LoadGame()
        {
            SceneManager.LoadScene(_gameScene);
        }
    
        public static void RestartGame()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

    }
}
