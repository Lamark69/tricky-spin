﻿using UnityEngine;

[CreateAssetMenu(fileName = "GameBehaviourSettings", menuName = "ScriptableObjects/GameBehaviourSettings", order = 1)]
public class GameBehaviourSettings : ScriptableObject
{
    public float DelayGameOverDialog = 1;
}
