﻿using Pool;
using UnityEngine;
using Game.Events;

namespace Services
{
    public class GameFX : MonoBehaviour
    {
        [SerializeField] private SimplePool _playerCollect;

        private void Awake()
        {
            _playerCollect.Initialize();
            Events.Subscribe<GameEvents.PlayerCollect>(OnPlayerCollect);
        }

        private void OnPlayerCollect(object arg)
        {
            var eventArg = arg as GameEvents.PlayerCollect;
            GameObject fx = _playerCollect.TakeObject().gameObject;
            
            fx.transform.position = eventArg.Obstacle.gameObject.transform.position;
            
            IPoolHandle handle = fx.GetComponent<IPoolHandle>();
            handle.Pool = _playerCollect;
        }
    }
}
