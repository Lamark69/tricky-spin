﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Helpers
{
    public static class InterfaceHelper
    {
        public static List<T> FindInterfaces<T>()
        {
            List<GameObject> allObjects = GameObject.FindObjectsOfType<GameObject>().ToList();

            List<T> interfaces = allObjects.Where(go => go.GetComponent<T>() != null)
                .Select(go => go.GetComponent<T>()).ToList();
        
            return interfaces;
        }
    
        public static T FindInterface<T>()
        {
            return FindInterfaces<T>().First();
        }

    }
}
