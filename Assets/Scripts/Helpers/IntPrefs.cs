﻿using UnityEngine;
using System.Collections;

namespace Helpers
{
    public class IntPrefs
    {
        public int Value
        {
            get
            {
                if (PlayerPrefs.HasKey(this._tag))
                    return PlayerPrefs.GetInt(this._tag, _startValue);
                else return _startValue;
            }
            set
            {
                PlayerPrefs.SetInt(_tag, value);
                PlayerPrefs.Save();
            }
        }
        private string _tag;
        private int _startValue;

        public IntPrefs(string tag, int startValue)
        {
            this._tag = tag;
            this._startValue = startValue;
        }

    }
}
